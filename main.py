from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Coroutine, Optional, List
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder


app = FastAPI()
app.title = "Mi primera chamba con APIS"

Base.metadata.create_all(bind=engine)

class User(BaseModel):
    email:str
    password:str

class JWTBearer(HTTPBearer):
    async def call(self, request: Request):
        auth = await super().call(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales Incorrectas")

class Movie(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    title: str = Field(min_length=5, max_length=15)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

    class Config:
        json_schema_extra = {
            "example":{
                "id": 1,
                "title": "Titulo Pelicula",
                "overview": "Descripcion de la pelicula",
                "year": 2023,
                "rating": 6.6,
                "category": "Accion"
            }
        }



movies = [
    {
        "id": 1,
        "title": "Avatar",
        "overview": "En un exuberante planeta llamado pandora viven unos monos azules",
        "year": "2009",
        "rating": 7.8,
        "category": "Accion"
    },
    {
        "id": 2,
        "title": "The Hunger Games on Fire",
        "overview": "Best Movie Ever",
        "year": "2014",
        "rating": 10.0,
        "category": "Accion"
    }
]

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')

@app.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)

@app.get('/movies', tags=['movies'], response_model= List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = db.query(MovieModel).all()
    return JSONResponse(status_code=200, content= jsonable_encoder(result))

@app.get('/movies/{id}', tags=['movies'])
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@app.get('/movies/', tags=['movies'])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.category == category).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@app.post('/movies', tags=['movies'])
def create_movie(movie: Movie) -> dict:
    db = Session()
    #Utilizamos el modelo y le pasamos la informacion que vamos a registrar
    new_movie = MovieModel(**movie.model_dump())
    #Ahora a la BD añadimos esa Pelicula
    db.add(new_movie)
    #Guardamos Datos
    db.commit()
    movies.append(movie)
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})

@app.put('/movies/{id}', tags=['movies'], response_model= dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'mesaage': 'No encontrado'})
    result.title = movie.title
    result.overview = movie.overview
    result.year = movie.year
    result.rating = movie.rating
    result.category = movie.category
    db.commit()
    return JSONResponse(status_code=200, content={'message':'Se ha modificado la pelicula'})

        
#@app.delete('/movies/{id}', tags=['movies'], response_model= dict, status_code=200)
#def delete_movie(id: int) -> dict:
    #db = Session()
    #result = db.query(MovieModel).filter(MovieModel.id == id).first()
    #if not result:
        #return JSONResponse(status_code=404, content={'mesaage': 'No encontrado'})
    #db.delete(result)
    #db.commit()
    #return JSONResponse(status_code=200, content={"message": "Se ha Eliminado la pelicula"})