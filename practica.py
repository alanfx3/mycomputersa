from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends #!Importacion de validación parametros
from fastapi.responses import HTMLResponse, JSONResponse #!
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Coroutine, Optional, List #!
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.computer import Computer as ComputerModel
from fastapi.encoders import jsonable_encoder

app = FastAPI()
app.title = "Venta de computadoras"

Base.metadata.create_all(bind=engine)

class User(BaseModel):
    email:str
    password:str

class JWTBearer(HTTPBearer):
    async def _call_(self, request: Request):
        auth = await super()._call_(request)
        data = validate_token(auth.credentials)
        if(data['email'] != "admin@gmail.com"):
            raise HTTPException(status_code=403, detail="Credenciales inválidas")
        
class Computer(BaseModel):
    #Puede ser opcional (None)
    id: Optional[int] = None 
    marca : str
    modelo : str 
    color : str
    ram : int = Field(ge=8)
    almacenamiento : str

    class Config:
        json_schema_extra = {
            "example":{
                "id": 1,
                "marca": "Mac",
                "modelo": "Macbook Air",
                "color": "Gris espacial",
                "ram": 8,
                "almacenamiento": "512 GB"
            }
        }

computers = []

@app.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)

#OBTENER TODAS LAS COMPUTADORAS
@app.get('/computers', tags=['computadoras'], response_model= List[Computer], status_code=200, dependencies=[Depends(JWTBearer())])
def get_computer_all() -> List[Computer]:
    db = Session()
    result = db.query(ComputerModel).all()
    return JSONResponse(status_code=200, content= jsonable_encoder(result))

#OBTENER COMPUTADORAS POR ID
@app.get('/computers/{id}', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def get_computer_id(id: int = Path(ge=1, le=2000)) -> Computer:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#OBTENER COMPUTADORAS POR MARCA
@app.get('/computers/', tags=['Computers'], dependencies=[Depends(JWTBearer())])
def get_by_marca(marca: str = Query(min_length=1, max_length=15)) -> List[Computer]:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.marca == marca).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@app.post('/computer/', tags=['computers'], response_model= List[Computer], status_code=200, dependencies=[Depends(JWTBearer())])
def post_computer(computadora: Computer) -> dict: 
    db = Session()
    new_computadora = ComputerModel(**computadora.dict())
    db.add(new_computadora)
    db.commit()
    computers.append(computadora)
    return JSONResponse(status_code=200, content={"message":"Se ha registrado la computadora"})

@app.delete('/computer/{id}', tags=['computers'], dependencies=[Depends(JWTBearer())])
def delete_computer(id: int) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': "Not found"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200, content={'message': "Se ha eliminado la computadora"})

@app.put('/computer/{id}', tags=['computers'], response_model= List[Computer], status_code=200)
def update_computer(id: int, computer: Computer) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': "Not found"})
    result.marca = computer.marca
    result.modelo = computer.modelo
    result.color = computer.color
    result.ram = computer.ram
    result.almacenamiento = computer.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={'message': "Se ha modificado la computadora"})