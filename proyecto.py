from fastapi import FastAPI, Query, Path
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List

# Crear instancia de la aplicación FastAPI
app = FastAPI()
app.title = "Biblioteca Digital"

# Definir el modelo Pydantic para Book
class Book(BaseModel):
    title: str
    autor: str
    year: int = Field(default=2000, le=2024)
    category: str = Field(min_length=5, max_length=15)     
    page_number: int = Field(ge=20)

    # Datos de ejemplo para la documentación de la API
    class Config:
        json_schema_extra = {
            "example": {
                "title": "Mi libro",
                "autor": "Autor del libro",
                "year": 2023,
                "category": "Fantasía",
                "page_number": 400,
            }
        }

# Variables globales
books = []
categorias = []
codigo_counter = 0

#################################### BUSQUEDA ################################################

# Ruta para obtener todos los libros
@app.get('/books', tags=['BÚSQUEDA'], response_model=List[Book], status_code=200)
def get_books() -> List[Book]:
    return JSONResponse(status_code=200, content=[book for book in books])

# Ruta para obtener un libro por su código
@app.get('/books/{codigo}', tags=['BÚSQUEDA'])
def get_book(codigo: int = Path(..., ge=1, le=2000)) -> Book:
    for item in books:
        if item["codigo"] == codigo:
            return JSONResponse(content=item)
    return JSONResponse(status_code=404, content={"message": "Libro no encontrado"})

# Ruta para obtener libros por categoría
@app.get('/books/', tags=['BÚSQUEDA'])
def get_book_by_category(category: str = Query(..., min_length=5, max_length=15)) -> List[Book]:
    filtered_books = [item for item in books if item['category'] == category]
    return JSONResponse(content=filtered_books)

##################################### CATEGORY ###############################################

# Ruta para crear una nueva categoría
@app.post('/categoria', tags=['CATEGORÍA'])
def create_category(categoria: str):
    if categoria not in categorias:
        categorias.append(categoria)
        return {"message": "Categoría agregada", "categoria": categoria}
    else:
        return JSONResponse(status_code=400, content={"message": "La categoría ya existe."})

# Ruta para eliminar una categoría
@app.delete('/categoria/{delete}', tags=['CATEGORÍA'])
def delete_category(categoria: str) -> dict:
    if any(book['category'] == categoria for book in books):
        return JSONResponse(status_code=400, content={"message": "No se puede eliminar la categoría porque hay libros asociados a ella."})
    elif categoria in categorias:
        categorias.remove(categoria)
        return {"message": f"Se ha eliminado la categoría: {categoria}"}
    else:
        return JSONResponse(status_code=404, content={"message": "La categoría especificada no existe."})

# Ruta para actualizar una categoría
@app.put('/categoria/{categoria}', tags=['CATEGORÍA'])
def update_category(categoria: str, nueva_categoria: str) -> dict:
    global books, categorias
    if categoria in categorias:
        for book in books:
            if book['category'] == categoria:
                categorias.remove(categoria)
                book['category'] = nueva_categoria
        if nueva_categoria not in categorias:
            categorias.append(nueva_categoria)
        return {"message": f"Se ha modificado la categoría '{categoria}' a '{nueva_categoria}' en todos los libros."}
    else:
        return JSONResponse(status_code=404, content={"message": "La categoría no existe. No se puede modificar la categoría en los libros."})
# Ruta para obtener todas las categorías
@app.get('/categorias', tags=['CATEGORÍA'], response_model=List[str])
def get_categories() -> List[str]:
    return categorias

############################################## ADMIN ############################################

# Ruta para crear un nuevo libro
@app.post('/books/', tags=['ADMIN'])
def create_book(book: Book) -> dict:
    if book.category not in categorias:
        return JSONResponse(status_code=400, content={"message": "La categoría no existe. No se puede registrar el libro a una categoría inexistente."})
    global codigo_counter
    codigo_counter += 1
    book_dict = book.dict()
    book_dict['codigo'] = codigo_counter
    books.append(book_dict)
    return JSONResponse(content={"message": "Se ha registrado el libro", "codigo": codigo_counter})

# Ruta para eliminar un libro por su código
@app.delete('/books/{codigo}', tags=['ADMIN'])
def delete_book(codigo: int) -> dict:
    global books
    books = [book for book in books if book["codigo"] != codigo]
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado el libro"})

# Ruta para actualizar un libro por su código
@app.put('/books/{codigo}', tags=['ADMIN'])
def update_book(codigo: int, updated_book: Book) -> dict:
    for index, book in enumerate(books):
        if book['codigo'] == codigo:
            if updated_book.category not in categorias:
                return JSONResponse(status_code=400, content={"message": "La categoría no existe. No se puede modificar el libro a una categoría inexistente."})
            update_data = updated_book.dict(exclude_unset=True)
            books[index].update(update_data)
            return JSONResponse(content={"message": "Se ha modificado el libro"})
    return JSONResponse(status_code=404, content={"message": "Libro no encontrado"})